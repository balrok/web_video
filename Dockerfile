FROM alpine:3.15 AS download
RUN apk add --no-cache curl && \
	curl -Ljo bento4.zip  https://www.bok.net/Bento4/binaries/Bento4-SDK-1-6-0-639.x86_64-unknown-linux.zip && \
	unzip bento4.zip && \
	mv Bento4-SDK-1-6-0-639.x86_64-unknown-linux/ bento4

FROM python:3.9 AS python_install
WORKDIR /app
RUN pip install --no-cache-dir poetry==1.1.2
COPY ./pyproject.toml ./poetry.lock ./
COPY ./web_video web_video
RUN poetry config virtualenvs.create true && \
	poetry config virtualenvs.in-project true && \
	poetry install --no-dev && \
	cp -rp .venv .venv-no-dev && \
	poetry install && \
	mv .venv .venv-dev


FROM python:3.9 AS production
WORKDIR /app
ENV DIR=/watchdir
# a url, which should be called after something in watchdir got converted
ENV CALLBACK=
# if RUN_ONCE has any value set (even false!), it will exit immediately after checking watchdir
ENV RUN_ONCE=
ENV PATH=${PATH}:/app/bento4/bin:/app/bento4/utils:/app/.venv/bin

RUN apt-get update && \
	apt-get install -y --no-install-recommends ffmpeg inotify-tools && \
	rm -rf /var/lib/apt/lists/*

COPY --from=download bento4/ bento4
COPY --from=python_install /app/.venv-no-dev .venv
COPY --from=python_install /app/web_video web_video
COPY docker/entrypoint.sh .

ENTRYPOINT [ "/app/entrypoint.sh" ]

FROM production AS test
ENTRYPOINT []
RUN apt-get update && \
	apt-get install -y --no-install-recommends make && \
	rm -rf /var/lib/apt/lists/*
COPY --from=python_install /app/.venv-dev .venv
COPY ./web web/
COPY ./tests tests/
COPY ./Makefile ./Makefile

