format:
	poetry run black ./web_video
	# removes unused imports
	poetry run autoflake -ri ./web_video
	poetry run isort ./web_video
	poetry run black ./tests
	poetry run isort ./tests

test:
	poetry run mypy ./web_video
	poetry run pyflakes ./web_video
	poetry run pytest -s

test_:
	mypy ./web_video
	pyflakes ./web_video
	pytest -s
