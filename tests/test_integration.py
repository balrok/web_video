"""This will perform an integrated test:
    * docker build
    * convert of some directories
        * check file-hashes
    * php webpage
        * check content
"""
import hashlib
import logging
import os
import pprint
import shutil
import subprocess
import sys
from pathlib import Path
from typing import Any, Dict, List, Union

from web_video.run import main

TEST_PATH = Path("tests")
ROOT_PATH = Path(".")


logger = logging.getLogger()
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
logger.setLevel(logging.DEBUG)
logger.addHandler(console_handler)


def _md5_file(filename: Path) -> str:
    hashmd5 = hashlib.md5()
    with filename.open("rb") as file:
        for chunk in iter(lambda: file.read(4096), b""):
            hashmd5.update(chunk)
    return hashmd5.hexdigest()


def _path_to_tree(path: Path) -> List[Dict[str, Any]]:
    tree = []
    for item in sorted(path.iterdir()):
        # pytest copies the test-files and we don't want it in our regression data
        if item.name.startswith("test_"):
            continue
        if item.is_dir():
            tree += [{"name": item.name, "subs": _path_to_tree(item)}]
        elif item.is_file():
            file = {
                "name": item.name,
            }
            # all ffmpeg things have changing hashes/sizes with each run
            # so just do this check for jpg
            if item.name.endswith(".jpg"):
                file["size"] = item.stat().st_size
                file["hash"] = _md5_file(item)
            tree += [file]
        else:
            tree += [
                {
                    "name": item.name,
                }
            ]
    return tree


def test_convert_dirs(tmp_path, data_regression):
    galleries_path = ROOT_PATH / "web/galleries"
    shutil.copytree(galleries_path, tmp_path, dirs_exist_ok=True)
    main(tmp_path, "", 0)
    dir_tree = _path_to_tree(tmp_path)
    data_regression.check(dir_tree, "convert_dirs")

    shutil.copy(tmp_path / "sample" / "img1" / "1024.jpg", tmp_path / "sample" / "new_img.jpg")
    os.utime(tmp_path / "sample" / "new_img.jpg", (0, 0))
    main(tmp_path, "", 0)
    dir_tree = _path_to_tree(tmp_path)
    data_regression.check(dir_tree, "convert_dirs2")
