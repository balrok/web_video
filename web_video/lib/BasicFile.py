import logging as log
import os
import re
import shutil
import unicodedata
from pathlib import Path
from shlex import quote
from subprocess import PIPE
from time import time
from typing import Callable, Dict, List, Tuple

from web_video.lib.helper import srun


class BasicFile(object):
    ERROR = 0
    UPDATED = 1
    NO_CHANGE = 2

    ### Static methods

    @staticmethod
    def test_requirements() -> bool:
        raise Exception("test_requirements must be implemented in child-class")

    @staticmethod
    def cmd_exists(program: str) -> bool:
        v = srun("which {}".format(program), stdout=PIPE)
        if not v or v.stdout == b"":
            log.error("Program {} does not exist".format(program))
            return False
        return True

    ### Instance methods
    def __init__(self, entry: Path) -> None:
        self.entry = entry
        self.name = entry.name
        self.path = entry.parent
        self.is_processed_folder = False

    def update(self) -> Dict[str, int]:
        raise Exception("Needs to be implemented")

    def getRunOpts(self) -> Dict[str, str]:
        name_no_ext, ext = os.path.splitext(os.path.basename(self.getOrigFile()))
        return {
            "o": quote(self.getOrigFile()),
            "oe": ext.lower(),
            "tf": quote(self.getTargetFolder()),
            "path": str(self.path.absolute()),
            "name": self.name,
        }

    @staticmethod
    def foldername_string(value: str):
        """
        Normalizes string, converts to lowercase, removes non-alpha characters,
        and converts spaces to hyphens.
        """
        value = unicodedata.normalize("NFKD", value).encode("ascii", "ignore").decode("utf-8")
        value = re.sub("[^\w\s-]", "", value).strip().lower()
        return re.sub("[-\s]+", "_", value)

    def getTargetPath(self) -> Path:
        return Path(self.getTargetFolder())

    def getTargetFolder(self):
        file = os.path.join(self.path, self.name)
        name_no_ext, ext = os.path.splitext(os.path.basename(self.getOrigFile()))
        if os.path.isdir(file):
            return file
        else:
            return os.path.join(self.path, BasicFile.foldername_string(name_no_ext))

    def getOrigFile(self):
        if os.path.isdir(os.path.join(self.path, self.name)):
            for e in self.exts:
                path = os.path.join(self.path, self.name, "orig." + e)
                if os.path.exists(path):
                    return path
            return False
        else:
            return os.path.join(self.path, self.name)

    def istype(self):
        return True

    def run_batches(self, items: List[Tuple[str, Callable]]) -> Dict[str, int]:
        if not self.getTargetPath().is_dir():
            return {"update": self.NO_CHANGE}
        ret = {}
        for i in items:
            try:
                ret[i[0]] = i[1]()
            except Exception:
                log.error("Could not execute batch-step %s", i[0], exc_info=True)
                ret[i[0]] = self.ERROR
        return ret

    def process(self) -> Tuple[bool, bool]:
        """returns [changed, error]"""
        # TODO is this needed?
        # if self.is_processed_folder:
        #    return self.update()
        if self.getTargetPath().exists():
            log.debug("%s: with type %s already converted", self.entry.name, self.__class__.__name__)
            return False, False

        log.info("%s: with type %s needs to be converted", self.entry.name, self.__class__.__name__)
        self.getTargetPath().mkdir(exist_ok=True)
        shutil.move(self.getOrigFile(), self.getTargetPath() / ("orig" + self.entry.suffix))
        self.name = self.getTargetPath().name
        start = time()
        res = self.update()

        changed = False
        error = False
        for i in res:
            if res[i] == self.UPDATED:
                changed = True
                log.info("updated %s - %s", self.entry.name, i)
            if res[i] == self.ERROR:
                log.error("ERROR: %s could not be processed - %s\n", self.entry.name, i)
                error = True
        if time() - start > 1:
            log.info("For %s took: %d", self.entry.name, int(time() - start))
        return changed, error
