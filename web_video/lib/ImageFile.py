import shutil
from pathlib import Path
from tempfile import TemporaryDirectory

from web_video.lib.BasicFile import BasicFile
from web_video.lib.helper import srun


class ImageFile(BasicFile):
    exts = ("jpg", "png")
    sizes = (2048, 1024, 512, 256)

    def istype(self):
        if not super().istype():
            return False
        if self.entry.suffix[1:].lower() in self.exts:
            return True
        if self.entry.is_dir() and self.getOrigFile():
            self.is_processed_folder = True
            return True
        return False

    def update(self):
        ret = [
            ("sizes", self.convert_sizes),
        ]
        return super().run_batches(ret)

    def convert_sizes(self):
        if (self.getTargetPath() / "256x.jpg").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            for s in self.sizes:
                srun(
                    "convert {o} -auto-orient -resize {size}x{size}\\> {tmp}/{size}.jpg".format(
                        size=s, tmp=temp_dir, **self.getRunOpts()
                    )
                )
                srun(
                    "convert {o} -auto-orient -resize x{size}\\> {tmp}/x{size}.jpg".format(
                        size=s, tmp=temp_dir, **self.getRunOpts()
                    )
                )
                srun(
                    "convert {o} -auto-orient -resize {size}x\\> {tmp}/{size}x.jpg".format(
                        size=s, tmp=temp_dir, **self.getRunOpts()
                    )
                )
            for img_file in (temp_path).glob("*"):
                shutil.move(img_file, self.getTargetFolder())
        return self.UPDATED

    @staticmethod
    def test_requirements():
        ret = True
        ret &= BasicFile.cmd_exists("convert")
        return ret
