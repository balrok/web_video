import json
import logging as log
import shutil
from pathlib import Path
from subprocess import PIPE, CalledProcessError
from tempfile import TemporaryDirectory

from web_video.lib.BasicFile import BasicFile
from web_video.lib.helper import srun


class VideoFile(BasicFile):
    exts = ("mkv", "mp4", "mts", "webm", "mpg", "mpeg")

    def istype(self):
        if not super().istype():
            return False
        if self.entry.suffix[1:].lower() in self.exts:
            return True
        if self.entry.is_dir() and self.getOrigFile():
            self.is_processed_folder = True
            return True
        return False

    def update(self):
        ret = [
            ("multi", self.convert_multi_bitrates),  # most important
            ("screenshot", self.convert_screenshots),  # second important
            ("hls", self.convert_hls),
            ("dash", self.convert_dash),
            ("webm", self.convert_webm),  # least important
            ("sprite", self.convert_sprite),  # nice to have
        ]
        return super().run_batches(ret)

    def convert_multi_bitrates(self):
        if (self.getTargetPath() / "video_00500.mp4").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            try:
                srun(
                    "mp4-dash-encode.py --verbose --bitrates 3 --audio-codec aac --output-dir {td}/v {o}".format(
                        td=temp_dir, **self.getRunOpts()
                    )
                )
            except CalledProcessError:
                return self.ERROR
            for video_file in (temp_path / "v").glob("video_*"):
                shutil.move(video_file, self.getTargetFolder())
        return self.UPDATED

    def convert_screenshots(self):
        if (self.getTargetPath() / "img_3.jpg").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            d = self.get_json()
            duration = int(float(d["streams"][0]["duration"]))
            srun(
                "ffmpeg -i {o} -vf fps={fps}/{dur} {td}/img_%d.jpg".format(
                    fps=5, dur=duration, td=temp_dir, **self.getRunOpts()
                )
            )
            for img_file in (temp_path).glob("img_*"):
                shutil.move(img_file, self.getTargetFolder())
        return self.UPDATED

    def get_json(self):
        p = srun(
            "ffprobe -v quiet -print_format json -show_streams {tf}/{v}".format(
                v="video_00500.mp4", **self.getRunOpts()
            ),
            universal_newlines=True,
            stdout=PIPE,
        )
        return json.loads(p.stdout, strict=False)

    def convert_sprite(self):
        if (self.getTargetPath() / "sprite.jpg").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            d = self.get_json()
            duration = int(float(d["streams"][0]["duration"]))
            srun(
                "ffmpeg -i {o} -vf fps={fps}/{dur} {td}/sprite_%d.jpg".format(
                    td=temp_dir, fps=10, dur=duration, **self.getRunOpts()
                )
            )
            for i in range(1, 11):
                srun(
                    "convert {td}/sprite_{i}.jpg -resize {size}x\\> {td}/sprite_{size}_{i}.jpg".format(
                        i=i, size=256, td=temp_dir
                    )
                )
            srun(
                "montage {tmp}/sprite_{size}_* -tile 10x1 -geometry 256x {tmp}/sprite.jpg".format(
                    size=256, tmp=temp_dir
                )
            )
            shutil.move(temp_path / "sprite.jpg", self.getTargetFolder())
        return self.UPDATED

    def convert_webm(self):
        if (self.getTargetPath() / "video.webm").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            srun(
                "ffmpeg -i {o} -speed 1 -c:v libvpx -b:v 1M -c:a libvorbis -vf \"scale='min(600,iw)':-1\" {td}/video.webm".format(
                    td=temp_dir, **self.getRunOpts()
                )
            )
            shutil.move(temp_path / "video.webm", self.getTargetFolder())
        return self.UPDATED

    def convert_dash(self):
        if (self.getTargetPath() / "stream.mpd").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            srun("mp4dash -o {t}/dash {tf}/video_0*.mp4".format(t=temp_dir, **self.getRunOpts()))
            for dash_file in (temp_path / "dash").glob("*"):
                shutil.move(dash_file, self.getTargetFolder())
        return self.UPDATED

    def convert_hls(self):
        if (self.getTargetPath() / "master.m3u8").exists():
            return self.NO_CHANGE
        with TemporaryDirectory() as temp_dir:
            temp_path = Path(temp_dir)
            srun("mp4hls -o {t}/hls {tf}/video_0*.mp4".format(t=temp_dir, **self.getRunOpts()))
            for hls_file in (temp_path / "hls").glob("*"):
                shutil.move(hls_file, self.getTargetFolder())
        return self.UPDATED

    # TODO is this working?
    @staticmethod
    def test_requirements():
        ret = True
        # ret &= BasicFile.cmd_exists("python2.7")
        ret &= BasicFile.cmd_exists("mp4dash")  # bento4
        ret &= BasicFile.cmd_exists("mp4-dash-encode.py")  # bento4
        ffmpeg = BasicFile.cmd_exists("ffmpeg")
        ret &= ffmpeg
        ret &= BasicFile.cmd_exists("montage")  # imagemagick
        if ffmpeg:
            v = srun("ffmpeg -version", stdout=PIPE)
            opts = [
                b"--enable-libx264",  # video for mp4
                b"--enable-libfdk-aac",  # audio for mp4
                b"--enable-libvpx",  # video for webm
                b"--enable-libvorbis",  # audio for webm
            ]
            opts_not_exist = [o.decode("utf-8") for o in opts if o not in v.stdout]
            if len(opts_not_exist) > 0:
                log.error("ffmpeg not compiled with {}".format(" ".join(opts_not_exist)))
                ret = False
        return ret
