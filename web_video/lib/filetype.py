from pathlib import Path
from typing import List, Type

from web_video.lib.BasicFile import BasicFile
from web_video.lib.ImageFile import ImageFile
from web_video.lib.VideoFile import VideoFile

filetypes = []  # type: List[Type[BasicFile]]
filetypes.append(ImageFile)
filetypes.append(VideoFile)


def test_requirements() -> bool:
    ret = True
    for f in filetypes:
        ret &= f.test_requirements()
    return ret


def get_instance(entry: Path):
    for f in filetypes:
        fh = f(entry)
        if fh.istype():
            return fh
    return None
