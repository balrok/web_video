import logging as log
from subprocess import DEVNULL, CalledProcessError, run
from time import time

# process_stdout = sys.stdout
PROCESS_STDOUT = DEVNULL


def srun(*args, **kwargs):
    if "shell" not in kwargs:
        kwargs["shell"] = True
    if "stdout" not in kwargs:
        kwargs["stdout"] = PROCESS_STDOUT
    if "stderr" not in kwargs:
        kwargs["stderr"] = PROCESS_STDOUT
    log.info("CMD: %s", " ".join(args))
    start = time()
    try:
        ret = run(*args, **kwargs, check=True)
        if time() - start > 1:
            log.info("Took %d seconds", int(time() - start))
        if ret.returncode != 0:
            log.error("A command returned an error code %d", ret.returncode)
            log.error(args)
            log.error(kwargs)
            return False
        return ret
    except (OSError, CalledProcessError):
        log.error("While running a command an exception occured: %s", exc_info=True)
        log.error(args)
        log.error(kwargs)
        return False
