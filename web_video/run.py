#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# check dir for files older 60s so we are sure to have complete transfered files
# then create a tmp dir for them
# put a screenshot
# use bento4 to create different qualities
#     hls
#     dash
#     webm
# when finished remove file and move tmp directory

import logging
import logging as log
import logging.handlers as loghandlers
import shutil
import sys
import urllib.request
from pathlib import Path
from time import time

from web_video.lib.BasicFile import BasicFile
from web_video.lib.filetype import get_instance
from web_video.lib.singleton import SingleInstance


def main(root_dir: Path, callback: str = "", min_modified_s: int = 60) -> int:
    return_code = 0
    log.info("Checking %s", root_dir)
    dirs = 0
    for path in root_dir.iterdir():
        if not path.is_dir():
            continue
        dirs += 1
        log.debug("Checking %s:", path.name)
        log.debug("------------")

        changed = False
        dirstart = time()
        is_watch_dir = False
        for entry in path.iterdir():
            fh = get_instance(entry)
            if not fh:
                continue
            last_modified_s = time() - entry.stat().st_mtime
            if last_modified_s < min_modified_s:
                log.info(
                    "%s: with type %s needs to be converted but modified %ds ago. It is configured to wait at least %ds.",
                    entry.name,
                    fh.__class__.__name__,
                    last_modified_s,
                    min_modified_s,
                )
                continue
            is_changed, is_error = fh.process()
            changed |= is_changed
            if is_error:
                return_code = -1
            is_watch_dir = True

        if is_watch_dir:
            if BasicFile.foldername_string(path.name) != path.name:
                log.warning("The Foldername is not nice for urls - fixing it")
                shutil.move(path, path.parent / BasicFile.foldername_string(path.name))

            if time() - dirstart > 1:
                log.info("For dir %s took: %d", path.parent, int(time() - dirstart))

            if changed:
                if len(callback) > 0:
                    if callback.startswith("http"):
                        log.info("Opening url %s", callback)
                        with urllib.request.urlopen(callback) as cburl:
                            cburl.read()
                    else:
                        log.error("callback must start with http: %s", callback)
    log.info("Looked at %d dirs", dirs)
    return return_code


def configure_logging(root_dir: Path):
    logger = logging.getLogger()
    hdlr = loghandlers.RotatingFileHandler(
        root_dir / ".web_video.log",
        maxBytes=1024 * 1024 * 10,
        backupCount=5,
    )
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(console_handler)
    try:
        from logging.handlers import SysLogHandler

        syslog = SysLogHandler()
        syslog.setFormatter(formatter)
        logger.addHandler(syslog)
    except Exception as e:
        log.warning("No syslog handler: %s", e)


lock = None


def run_it():
    global lock
    root_dir = Path(sys.argv[1])
    configure_logging(root_dir)
    lock = SingleInstance("videotransformer")
    # if not test_requirements():
    #    log.error("Some program requirements not met")
    #    sys.exit(-1)
    # first argument is folder which we will check
    # second argument a callback-url
    exit_code = main(root_dir, *sys.argv[2:])
    sys.exit(exit_code)


if __name__ == "__main__":
    run_it()
